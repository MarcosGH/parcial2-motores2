using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class CUBO1 : MonoBehaviour
{
    public GameObject objectToMove;
    public Vector3 newPosition;

    private void OnCollisionEnter(Collision collision)
    {
        if (objectToMove != null)
        {
            objectToMove.transform.position = newPosition;
            UnityEngine.Debug.Log("Object moved to: " + newPosition);
        }
        else
        {
            UnityEngine.Debug.LogWarning("No object assigned to move.");
        }
    }
}