using UnityEngine;
using System.Collections;

public class Puerta77 : MonoBehaviour
{
    public Transform puntoFinal;
    public float velocidad = 1.0f;

    public void IniciarMovimiento()
    {
        StartCoroutine(MoverHaciaPuntoFinal());
    }

    private IEnumerator MoverHaciaPuntoFinal()
    {
        while (Vector3.Distance(transform.position, puntoFinal.position) > 0.01f)
        {
            transform.position = Vector3.MoveTowards(transform.position, puntoFinal.position, velocidad * Time.deltaTime);
            yield return null;
        }
    }
}