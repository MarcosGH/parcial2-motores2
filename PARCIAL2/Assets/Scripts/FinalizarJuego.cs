using UnityEngine;

public class FinalizarJuego : MonoBehaviour
{
    public Vector3 posicionFinal = new Vector3(42.11438f, 82.31584f, -74.35193f);

    public float distanciaAceptable = 0.1f;

    void Update()
    {
        float distancia = Vector3.Distance(transform.position, posicionFinal);

        if (distancia < distanciaAceptable)
        {
            Debug.Log("�Objeto ha llegado a la posici�n final!");
            FinalizarJuegoFunc();
        }
    }

    void FinalizarJuegoFunc()
    {
        Application.Quit();
    }
}