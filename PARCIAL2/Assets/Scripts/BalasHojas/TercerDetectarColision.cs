using UnityEngine;

public class TercerDetectarColision : MonoBehaviour
{
    public Transform objetoAMover;
    public float alturaMovimiento = 5f;
    public float velocidadMovimiento = 1.0f;

    private void OnCollisionEnter(Collision collision)
    {
        Bala3 bala3 = collision.gameObject.GetComponent<Bala3>();

        if (bala3 != null && objetoAMover != null)
        {
            Vector3 nuevaPosicion = objetoAMover.position + new Vector3(0f, alturaMovimiento, 0f);

            StartCoroutine(MoverObjetoSuavemente(objetoAMover.position, nuevaPosicion));
        }
    }

    private System.Collections.IEnumerator MoverObjetoSuavemente(Vector3 posicionInicial, Vector3 posicionFinal)
    {
        float distanciaTotal = Vector3.Distance(posicionInicial, posicionFinal);
        float distanciaRecorrida = 0f;

        while (distanciaRecorrida < distanciaTotal)
        {
            objetoAMover.position = Vector3.MoveTowards(objetoAMover.position, posicionFinal, velocidadMovimiento * Time.deltaTime);
            distanciaRecorrida += velocidadMovimiento * Time.deltaTime;
            yield return null;
        }

        objetoAMover.position = posicionFinal;
    }
}