using UnityEngine;

public class VerdeActivador : MonoBehaviour
{
    public string etiquetaObjetoVerde = "ObjetoVerde";
    public string etiquetaObjetoVerdeMover = "ObjetoVerdeMover";
    public Transform posicionInicial; 
    public Transform posicionFinal;   
    public float velocidadMovimiento = 5f;

    private bool placaActivada = false;

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(etiquetaObjetoVerde))
        {
            placaActivada = true;
        }
    }

    private void Update()
    {
        if (placaActivada)
        {
            GameObject[] objetosAMover = GameObject.FindGameObjectsWithTag(etiquetaObjetoVerdeMover);

            foreach (GameObject objetoAMover in objetosAMover)
            {
                Vector3 direccion = (posicionFinal.position - posicionInicial.position).normalized;

                Vector3 nuevaPosicion = objetoAMover.transform.position + direccion * velocidadMovimiento * Time.deltaTime;

                objetoAMover.transform.position = nuevaPosicion;

                float distancia = Vector3.Distance(objetoAMover.transform.position, posicionFinal.position);

                if (distancia < 0.1f) 
                {
                    placaActivada = false;
                }
            }
        }
    }
}
