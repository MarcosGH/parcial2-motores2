using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreguntaCuatro : MonoBehaviour
{
    public GameObject PreguntaVisual;
    public bool activa;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E) && activa == true)
        {
            PreguntaVisual.SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.Tab) && activa == true)
        {
            PreguntaVisual.SetActive(false);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Jugador")
        {
            activa = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Jugador")
        {
            activa = false;
            PreguntaVisual.SetActive(false);
        }
    }
}