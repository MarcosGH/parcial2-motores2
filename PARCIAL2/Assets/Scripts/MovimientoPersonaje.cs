using UnityEngine;

public class MovimientoPersonaje : MonoBehaviour
{
    public float velocidad = 6.0f;
    public float velocidadCarrera = 12.0f;
    public float fuerzaSalto = 8.0f;
    public Camera camara;
    public string tagObjetoDestino1 = "ObjetoDestino1";
    public string tagObjetoDestino2 = "ObjetoDestino2";

    private Vector3 direccion;
    private bool enSuelo = true;
    private Rigidbody rb;
    private Vector3 posicionInicial;
    private Vector3 puntoDeDestino1;
    private Vector3 puntoDeDestino2;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        if (camara == null)
        {
            camara = Camera.main;
        }

        posicionInicial = transform.position;

        puntoDeDestino1 = new Vector3(-10.769f, 79.541f, 15.313f);
        puntoDeDestino2 = new Vector3(1.351f, 104.4f, -11.915f);

        GestorDeAudio.instancia.ReproducirSonido("musica");
    }

    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        bool correr = Input.GetKey(KeyCode.LeftShift);

        Vector3 camaraForward = Vector3.Scale(camara.transform.forward, new Vector3(1, 0, 1)).normalized;
        direccion = v * camaraForward + h * camara.transform.right;

        if (Input.GetButtonDown("Jump") && enSuelo)
        {
            Saltar();
        }

        if (correr)
        {
            direccion *= velocidadCarrera;
        }
        else
        {
            direccion *= velocidad;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            ResetearPosicion();
        }
    }

    void FixedUpdate()
    {
        MoverPersonaje();
    }

    void MoverPersonaje()
    {
        rb.MovePosition(transform.position + direccion * Time.deltaTime);
    }

    void Saltar()
    {
        rb.velocity = new Vector3(rb.velocity.x, fuerzaSalto, rb.velocity.z);
        enSuelo = false;
    }

    void ResetearPosicion()
    {
        transform.position = posicionInicial;
        enSuelo = true;
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.CompareTag("Suelo") || other.gameObject.layer == LayerMask.NameToLayer("Suelo"))
        {
            enSuelo = true;
        }

        if (other.gameObject.CompareTag(tagObjetoDestino1))
        {
            MoverAdestino(puntoDeDestino1);
        }

        if (other.gameObject.CompareTag(tagObjetoDestino2))
        {
            MoverAdestino(puntoDeDestino2);
        }
    }

    void MoverAdestino(Vector3 destino)
    {
        transform.position = destino;
        enSuelo = true;
    }
}