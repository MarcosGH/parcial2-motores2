using UnityEngine;

public class PintarObjetoAlImpactar : MonoBehaviour
{
    public Color colorAlImpactar = Color.green;
    public string tagObjetivo = "Pintar";

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(tagObjetivo))
        {
            PintarObjeto(collision.gameObject);
        }
    }

    private void PintarObjeto(GameObject objeto)
    {
        Renderer rend = objeto.GetComponent<Renderer>();
        if (rend != null)
        {
            rend.material.color = colorAlImpactar;
        }
    }
}