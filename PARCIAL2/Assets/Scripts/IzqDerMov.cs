using UnityEngine;

public class IzqDerMov : MonoBehaviour
{
    public float velocidad = 5f; 

    void Update()
    {
        transform.Translate(Vector3.right * velocidad * Time.deltaTime);

        if (transform.position.x >= -6.60f) 
        {
            velocidad = -velocidad; 
        }
        else if (transform.position.x <= -13f) 
        {
            velocidad = Mathf.Abs(velocidad); 
        }
    }
}
