using UnityEngine;

public class DetectarColision : MonoBehaviour
{
    public string nombreObjeto2 = "Activador77";

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == nombreObjeto2)
        {
            Debug.Log("El objeto1 ha tocado el objeto2.");
        }
    }
}