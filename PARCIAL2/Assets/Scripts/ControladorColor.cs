using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ControladorColor : MonoBehaviour
{
    public string[] combinacionDeseada = { "Violeta", "Verde", "Rojo", "Blanco", "Amarillo", "Azul", "AzulOscuro", "Naranja", "Gris", "Negro", "Marron", "Celeste" };
    private int indiceActual = 0;

    public Material materialCorrecto;
    public Material materialIncorrecto;

    public GameObject cuboAMover;
    public Vector3 nuevaPosicionCubo;

    public List<AudioClip> sonidosPorBloque;

    private List<ObjetoConMaterialOriginal> objetosCambiados = new List<ObjetoConMaterialOriginal>();
    private AudioSource audioSource;

    private struct ObjetoConMaterialOriginal
    {
        public GameObject objeto;
        public Material materialOriginal;
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        string etiquetaColision = collision.collider.tag;

        if (EsColorCorrecto(etiquetaColision))
        {
            CambiarMaterial(collision.collider.gameObject, materialCorrecto);
            ReproducirSonidoCorrecto();
            indiceActual++;

            if (indiceActual == combinacionDeseada.Length)
            {
                MoverCubo();
                ReiniciarCombinacion();
            }
        }
        else
        {
            foreach (ObjetoConMaterialOriginal objetoCambiado in objetosCambiados)
            {
                RestaurarMaterial(objetoCambiado);
            }

            objetosCambiados.Clear();
            ReiniciarCombinacion();
        }
    }

    private bool EsColorCorrecto(string etiqueta)
    {
        return indiceActual < combinacionDeseada.Length && etiqueta == combinacionDeseada[indiceActual];
    }

    private void ReiniciarCombinacion()
    {
        indiceActual = 0;
    }

    private void CambiarMaterial(GameObject obj, Material nuevoMaterial)
    {
        Renderer renderer = obj.GetComponent<Renderer>();

        if (renderer != null)
        {
            Material materialOriginal = renderer.material;

            renderer.material = nuevoMaterial;

            objetosCambiados.Add(new ObjetoConMaterialOriginal { objeto = obj, materialOriginal = materialOriginal });
        }
    }

    private void RestaurarMaterial(ObjetoConMaterialOriginal objetoConMaterial)
    {
        Renderer renderer = objetoConMaterial.objeto.GetComponent<Renderer>();

        if (renderer != null)
        {
            renderer.material = objetoConMaterial.materialOriginal;
        }
    }

    private void MoverCubo()
    {
        StartCoroutine(MoverHaciaDestino(cuboAMover.transform, nuevaPosicionCubo));
    }

    private void ReproducirSonidoCorrecto()
    {
        if (audioSource != null && sonidosPorBloque != null && sonidosPorBloque.Count > 0 && indiceActual < sonidosPorBloque.Count)
        {
            audioSource.PlayOneShot(sonidosPorBloque[indiceActual]);
        }
    }

    private IEnumerator MoverHaciaDestino(Transform objetoTransform, Vector3 destino)
    {
        float tiempoInicio = Time.time;
        Vector3 posicionInicio = objetoTransform.position;
        float duracion = 4.0f;

        while (Time.time - tiempoInicio < duracion)
        {
            float tiempoPasado = Time.time - tiempoInicio;
            float fraccionCompleta = tiempoPasado / duracion;

            objetoTransform.position = Vector3.Lerp(posicionInicio, destino, fraccionCompleta);
            yield return null;
        }

        objetoTransform.position = destino;
    }
}