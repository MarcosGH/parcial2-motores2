using UnityEngine;

public class ClickBoton : MonoBehaviour
{
    public GameObject objetoAMover;
    public Vector3 posicionFinal;
    public float velocidad = 5.0f;
    public AudioClip sonidoClic;  
    private Vector3 posicionInicial;
    private bool moviendo = false;
    private AudioSource audioSource;

    void Start()
    {
        posicionInicial = objetoAMover.transform.position;

        audioSource = gameObject.AddComponent<AudioSource>();
        audioSource.clip = sonidoClic;
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit) && hit.collider.gameObject == gameObject)
            {
                moviendo = true;

                if (audioSource != null && sonidoClic != null)
                {
                    audioSource.PlayOneShot(sonidoClic);
                }
            }
        }

        if (moviendo)
        {
            objetoAMover.transform.position = Vector3.Lerp(objetoAMover.transform.position, posicionFinal, velocidad * Time.deltaTime);

            if (Vector3.Distance(objetoAMover.transform.position, posicionFinal) < 0.01f)
            {
                moviendo = false;
            }
        }
    }
}