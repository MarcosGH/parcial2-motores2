using UnityEngine;

public class AgarreObjetos : MonoBehaviour
{
    public float distanciaAgarreMaxima = 5f;
    public LayerMask capaObjetos;
    public Transform puntoAgarre;
    public GameObject objetoAgarrado;
    private bool estaAgarrando = false;
    public float distanciaObjetoAPuntoDeAgarre = 2f;

    public GameObject balaPrefabArma1;
    public GameObject balaPrefabArma2;
    public GameObject balaPrefabArma3;

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            if (!estaAgarrando)
            {
                AgarrarObjeto();
            }
            else
            {
                SoltarObjeto();
            }
        }

        if (estaAgarrando && objetoAgarrado != null) 
        {
            MoverObjetoConCursor();

            if (objetoAgarrado.CompareTag("Arma"))
            {
                if (Input.GetButtonDown("Fire2"))
                {
                    DispararArma();
                }
            }
            else if (objetoAgarrado.CompareTag("Arma2"))
            {
                if (Input.GetButtonDown("Fire2"))
                {
                    DispararArma2();
                }
            }
            else if (objetoAgarrado.CompareTag("Arma3"))
            {
                if (Input.GetButtonDown("Fire2"))
                {
                    DispararArma3();
                }
            }
        }
    }

    void AgarrarObjeto()
    {
        RaycastHit hit;

        Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));

        if (Physics.Raycast(ray, out hit, distanciaAgarreMaxima, capaObjetos))
        {
            objetoAgarrado = hit.collider.gameObject;

            if (objetoAgarrado != null)
            {

                if (objetoAgarrado.CompareTag("Arma") || objetoAgarrado.CompareTag("Arma2") || objetoAgarrado.CompareTag("Arma3"))
                {
                    objetoAgarrado.transform.rotation = Quaternion.identity;
                }

                Collider objetoCollider = objetoAgarrado.GetComponent<Collider>();
                if (objetoCollider != null)
                {
                    objetoCollider.enabled = false;
                }

                Rigidbody objetoRigidbody = objetoAgarrado.GetComponent<Rigidbody>();
                if (objetoRigidbody != null)
                {
                    objetoRigidbody.isKinematic = true;
                }

                estaAgarrando = true;

            }
        }
    }

    public void SoltarObjeto()
    {
        if (objetoAgarrado != null)
        {
            objetoAgarrado.GetComponent<Collider>().enabled = true;
            objetoAgarrado.GetComponent<Rigidbody>().isKinematic = false;
            objetoAgarrado = null;
            estaAgarrando = false;
        }
    }

    void MoverObjetoConCursor()
    {
        if (objetoAgarrado != null)
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0));
            Vector3 direccion = (ray.GetPoint(distanciaAgarreMaxima) - puntoAgarre.position).normalized;
            Vector3 nuevaPosicion = puntoAgarre.position + direccion * distanciaObjetoAPuntoDeAgarre;

            objetoAgarrado.transform.position = nuevaPosicion;
        }
    }

    void DispararArma()
    {
        if (objetoAgarrado != null && objetoAgarrado.CompareTag("Arma"))
        {
            Vector3 direccionDisparo = Camera.main.transform.forward;

            GameObject bala = Instantiate(balaPrefabArma1, objetoAgarrado.transform.position, objetoAgarrado.transform.rotation);

            bala.AddComponent<DestruirEnColision>();

            Rigidbody balaRigidbody = bala.GetComponent<Rigidbody>();
            balaRigidbody.velocity = direccionDisparo * 10f;

            PintarObjetoAlImpactar pintarObjetoScript = bala.GetComponent<PintarObjetoAlImpactar>();
            if (pintarObjetoScript != null)
            {
                pintarObjetoScript.colorAlImpactar = objetoAgarrado.GetComponent<Renderer>().material.color;
            }
        }
    }

    void DispararArma2()
    {
        if (objetoAgarrado != null && objetoAgarrado.CompareTag("Arma2"))
        {
            Vector3 direccionDisparo = Camera.main.transform.forward;

            GameObject bala = Instantiate(balaPrefabArma2, objetoAgarrado.transform.position, objetoAgarrado.transform.rotation);

            bala.AddComponent<DestruirEnColision>();

            Rigidbody balaRigidbody = bala.GetComponent<Rigidbody>();
            balaRigidbody.velocity = direccionDisparo * 10f;

            PintarObjetoAlImpactar pintarObjetoScript = bala.GetComponent<PintarObjetoAlImpactar>();
            if (pintarObjetoScript != null)
            {
                pintarObjetoScript.colorAlImpactar = objetoAgarrado.GetComponent<Renderer>().material.color;
            }
        }
    }

    void DispararArma3()
    {
        if (objetoAgarrado != null && objetoAgarrado.CompareTag("Arma3"))
        {
            Vector3 direccionDisparo = Camera.main.transform.forward;

            GameObject bala = Instantiate(balaPrefabArma3, objetoAgarrado.transform.position, objetoAgarrado.transform.rotation);

            bala.AddComponent<DestruirEnColision>();

            Rigidbody balaRigidbody = bala.GetComponent<Rigidbody>();
            balaRigidbody.velocity = direccionDisparo * 10f;

            PintarObjetoAlImpactar pintarObjetoScript = bala.GetComponent<PintarObjetoAlImpactar>();
            if (pintarObjetoScript != null)
            {
                pintarObjetoScript.colorAlImpactar = objetoAgarrado.GetComponent<Renderer>().material.color;
            }
        }
    }
}