using UnityEngine;

public class CambiadorMusica : MonoBehaviour
{
    public AudioClip nuevaMusica;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Jugador"))
        {
            CambiarMusica();
        }
    }

    private void CambiarMusica()
    {
        GestorDeAudio gestorDeAudio = GestorDeAudio.instancia;

        if (gestorDeAudio != null && nuevaMusica != null)
        {
            gestorDeAudio.PausarSonido("musica");

            gestorDeAudio.ReproducirSonido("musica2");
        }
    }
}