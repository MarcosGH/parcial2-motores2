using UnityEngine;


public class ControlCamara : MonoBehaviour
{
    public GameObject Jugador;
    public Vector3 offset;
    public float sensibilidad = 3.0f;
    private float rotX = 0.0f;
    private float rotY = 0.0f;

    void Start()
    {
        offset = transform.position - Jugador.transform.position;
    }

    void Update()
    {
        rotX += Input.GetAxis("Mouse X") * sensibilidad;
        rotY += Input.GetAxis("Mouse Y") * sensibilidad;
        rotY = Mathf.Clamp(rotY, -90, 90); 

        transform.localRotation = Quaternion.Euler(-rotY, rotX, 0); 
    }

    void LateUpdate()
    {
        transform.position = Jugador.transform.position + offset;
    }
}
