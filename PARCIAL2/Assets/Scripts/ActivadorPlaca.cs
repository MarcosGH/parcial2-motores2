using UnityEngine;

public class ActivadorPlaca : MonoBehaviour
{
    public string etiquetaObjetoEspecial = "ObjetoEspecial";
    public string etiquetaObjetoAMover = "ObjetoAMover";
    public float velocidadMovimiento = 5f;
    public float distanciaObjetivo = 5f;
    public AudioClip sonidoActivacion;
    private bool placaActivada = false;
    private float distanciaRecorrida = 0f;
    private AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag(etiquetaObjetoEspecial))
        {
            placaActivada = true;
            ReproducirSonidoActivacion();
        }
    }

    private void Update()
    {
        if (placaActivada)
        {
            GameObject[] objetosAMover = GameObject.FindGameObjectsWithTag(etiquetaObjetoAMover);

            foreach (GameObject objetoAMover in objetosAMover)
            {
                Vector3 nuevaPosicion = objetoAMover.transform.position + Vector3.left * velocidadMovimiento * Time.deltaTime;

                distanciaRecorrida += Mathf.Abs(nuevaPosicion.x - objetoAMover.transform.position.x);

                if (distanciaRecorrida < distanciaObjetivo)
                {
                    objetoAMover.transform.position = nuevaPosicion;
                }
                else
                {
                    placaActivada = false;
                    distanciaRecorrida = 0f;
                }
            }
        }
    }

    private void ReproducirSonidoActivacion()
    {
        if (sonidoActivacion != null && audioSource != null)
        {
            audioSource.PlayOneShot(sonidoActivacion);
        }
    }
}