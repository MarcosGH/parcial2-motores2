using UnityEngine;

public class ReproductorSonido : MonoBehaviour
{
    private AudioSource audioSource;

    private void Start()
    {
        audioSource = GetComponent<AudioSource>();

        if (audioSource == null || audioSource.clip == null)
        {
            Debug.LogError("El componente AudioSource o el clip de audio no est�n configurados correctamente.");
        }
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0)) 
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                if (hit.collider.gameObject == gameObject)
                {
                    audioSource.Play();
                }
            }
        }
    }
}
