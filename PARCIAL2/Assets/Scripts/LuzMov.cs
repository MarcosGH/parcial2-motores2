using UnityEngine;

public class LuzMov : MonoBehaviour
{
    public float velocidadRotacion = 30.0f;

    void Update()
    {
        transform.Rotate(Vector3.up, velocidadRotacion * Time.deltaTime);
    }
}
