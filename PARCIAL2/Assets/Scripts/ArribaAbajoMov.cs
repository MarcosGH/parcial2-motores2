using UnityEngine;

public class ArribaAbajoMov : MonoBehaviour
{
    public float velocidad = 5f; 

    void Update()
    {
        
        transform.Translate(Vector3.up * velocidad * Time.deltaTime);

        
        if (transform.position.y >= 79f) 
        {
            velocidad = -velocidad; 
        }
        else if (transform.position.y <= 74f) 
        {
            velocidad = Mathf.Abs(velocidad); 
        }
    }
}
