using UnityEngine;

public class DetectarColision1 : MonoBehaviour
{
    private void OnCollisionEnter(Collision collision)
    {
        Bala1 bala1 = collision.gameObject.GetComponent<Bala1>();

        if (bala1 != null)
        {
            Debug.Log("ˇObjetoPrincipal fue impactado por la Bala1!");
        }
    }
}